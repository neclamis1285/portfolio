module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-project-update');
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-filerev');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-text-replace');
    grunt.renameTask('replace', 'textReplace');

    grunt.loadNpmTasks('grunt-replace');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Variables
        jsResources: [],
        cssResources: [],

        // Create Directory Structure
        mkdir: {
            all: {
                options: {
                    create: [
                        'app',
                        'app/images',
                        'app/js',
                        'app/js/config',
                        'app/js/controller',
                        'app/js/main',
                        'app/js/service',
                        'app/js/state',
                        'app/lib',
                        'app/lib/bower',
                        'app/lib/other',
                        'app/sass',
                        'app/view',
                        'resources',
                        'resources/sql',
                        'server'
                    ]
                }
            },
            build: {
                options: {
                    create: ['build']
                }
            }
        },

        // Update/Install NPM And Bower Packages
        projectUpdate: {
            update: {
                npm: true,
                bower: true
            }
        },

        // Add Bower Packages To HTML And CSS File
        wiredep: {
            task: {
                src: [
                    'app/*.html',
                    'app/sass/app.scss'
                ],
                options: {
                    exclude: ['/angular/', '/angular-animate/']
                }
            }
        },

        // Clean Build Folder
        clean: {
            build: {
                src: ['build', 'index.html']
            },
            stylesheets: {
                src: ['build/**/*.css', '!build/app.min.css']
            },
            scripts: {
                src: ['build/**/*.js', '!build/app.min.js']
            }
        },

        // Get All JS and CSS References In HTML File
        replace: {
            gather: {
                files: [
                    {
                        cwd: 'app',
                        dest: 'build/',
                        expand: true,
                        src: ['index.html']
                    }
                ],
                options: {
                    patterns: [
                        {
                            //Grab the <!--build-js-start--> and <!--build-js-end--> comments and everything in-between
                            match: /\<\!\-\-build\-js\-start[\s\S]*build\-js\-end\-\-\>/,
                            replacement: function (matchedString) {
                                //Grab all of the src attributes from the <script> tags
                                var jsArray = matchedString.match(/(src\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/g);
                                jsArray.forEach(function (element) {
                                    //Get just the value of the src attribute (the file path to the JS file)
                                    var resourceTarget = element.match(/(src\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/)[2];
                                    targetConfig = grunt.config('jsResources');
                                    //Alter the path for use with the concat task
                                    targetConfig.push('app/' + resourceTarget);
                                    //Add the path to the JS file to the jsResources configuration property
                                    grunt.config('jsResources', targetConfig);
                                });

                                //Replace the entire build-js-start to build-js-end block with this <script> tag
                                return '<script type="text/javascript" src="build/app.min.js"></script>';
                            }
                        },
                        {
                            //Grab the <!--build-css-start--> and <!--build-css-end--> comments and everything in-between
                            match: /\<\!\-\-build\-css\-start[\s\S]*build\-css\-end\-\-\>/,
                            replacement: function (matchedString) {
                                //Grab all of the href attributes from the <href> tags
                                var cssArray = matchedString.match(/(href\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/g);
                                cssArray.forEach(function (element) {
                                    var resourceTarget = element.match(/(href\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/)[2];
                                    var targetConfig = grunt.config('cssResources');
                                    //Alter the path for use with the concat task
                                    targetConfig.push('app/' + resourceTarget);
                                    //Add the path to the CSS file to the cssResources configuration property
                                    grunt.config('cssResources', targetConfig);
                                });

                                //Replace the entire build-css-start to build-css-end block with this <link> tag
                                return '<link rel="stylesheet" media="screen" href="build/app.min.css"/>';
                            }
                        }
                    ]
                }
            }
        },

        // Copy Index File To Destination
        copy: {
            generated: {
                src: 'build/index.html',
                dest: 'index.html'
            }
        },

        // Change Reference Paths
        textReplace: {
            referenced: {
                src: [
                    'index.html'
                ],
                dest: 'index.html',
                replacements: [
                    {
                        from: /lib/g,
                        to: 'app/lib'
                    },
                    {
                        from: /sass/g,
                        to: 'app/sass'
                    }
                ]
            }
        },

        // Combine All JS and CSS Files Into Single JS And CSS File
        concat: {
            js: {
                //Concatenate all of the files in the jsResources configuration property
                src: ['<%= jsResources %>'],
                dest: 'build/combined.js',
                options: {
                    separator: ';'
                }
            },

            css: {
                //Concatenate all of the files in the cssResources configuration property
                src: ['<%= cssResources %>'],
                dest: 'build/combined.css'
            }

        },

        // Minify CSS Files
        cssmin: {
            build: {
                files: {
                    'build/app.min.css': ['build/**/*.css']
                }
            }
        },

        // Minify JS Files
        uglify: {
            build: {
                options: {
                    mangle: false
                },
                files: {
                    'build/app.min.js': ['build/**/*.js']
                }
            }
        },

        // Minify HTML File
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    'index.html': 'index.html'
                }
            }
        }
    });

    grunt.registerTask('writeLog', function () {
        console.log(grunt.config('jsResources'));
    });

    grunt.registerTask('default', [
        'mkdir:build',
        'projectUpdate'
    ]);

    grunt.registerTask('build', [
        'clean:build',
        'replace:gather',
        'concat:css',
        'concat:js',
        'cssmin',
        'uglify',
        'copy:generated',
        'textReplace:referenced',
        'clean:stylesheets',
        'clean:scripts',
        'htmlmin'
    ]);
};
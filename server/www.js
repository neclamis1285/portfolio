/**
 * Created by bernardchung on 10/5/15.
 */
"use strict";

// region Requires

var path = require("path");
var express = require("express");
var serverRoot = __dirname;
var applicationRoot = path.join(serverRoot, "..");
var bcrypt = require("bcryptjs");
var async = require("async");

// endregion

// region Server Setup

// Create Server
var app = express();

// Configure Server
app.configure(function () {
    // Parses request body and populates request.body
    app.use(express.bodyParser());

    // Checks request.body for HTTP method overrides
    app.use(express.methodOverride());

    // Perform route lookup based on url and HTTP method
    app.use(app.router);

    // Show all errors in development
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));

    // Add Angular App Root
    app.use('/app', express.static("app"));
    app.use('/build', express.static("build"));
});

// Start Server
var port = 4010;
app.listen(port, function () {
    console.log("Express server listening on port %d in %s mode", port, app.settings.env);
});

// endregion

// region Application Functions

// Get Landing page
app.get("/", function (request, response) {

    response.sendfile(path.join(applicationRoot + "/index.html"));

});

// endregion
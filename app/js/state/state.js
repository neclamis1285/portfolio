/**
 * Created by bernardchung on 10/5/15.
 */
'use strict';

portfolio.config([
    '$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/aboutMe');

        $stateProvider
            // General
            .state('aboutMe', {
                url: '/aboutMe',
                templateUrl: 'app/view/main/aboutMe.html',
                controller: 'MainController'
            })
            .state('resume', {
                url: '/resume',
                templateUrl: 'app/view/main/resume.html',
                controller: 'MainController'
            })
            .state('portfolio', {
                url: '/portfolio',
                templateUrl: 'app/view/main/portfolio.html',
                controller: 'MainController'
            })
        ;
    }
]);
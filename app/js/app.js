/**
 * Created by bernardchung on 10/5/15.
 */
'use strict';

var portfolio = angular.module('portfolio', [
    'ngLocale',
    'ngResource',
    'ngSanitize',
    'ui.router',
    'ngMaterial',
    'ngDialog',
    'restangular',
    'ngStorage',
    'ui.bootstrap',
    'ngMask',
    'checklist-model',
    'smart-table',
    'snap',
    'ng.deviceDetector'
]);

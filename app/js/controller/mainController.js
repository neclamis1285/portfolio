/**
 * Created by bernardchung on 10/5/15.
 */
'use strict';

portfolio.controller('MainController', [
    '$rootScope', '$scope', '$sessionStorage', '$timeout', '$state', '$window', 'ngDialog', 'snapRemote', 'deviceDetector',
    function ($rootScope, $scope, $sessionStorage, $timeout, $state, $window, ngDialog, snapRemote, deviceDetector) {

        // region Variables

        $scope.largeDisplay = true;
        $scope.smallDisplay = false;
        $scope.opts = {
            disable: 'right',
            touchToDrag: false
        };

        // endregion

        // region Initialization

        // Check Window Width And Apply Mobile/Desktop Styling
        function init() {
            var newWidth = window.innerWidth;

            if (newWidth < 700) {
                $scope.smallDisplay = true;
            } else if (newWidth < 1199) {
                $scope.largeDisplay = false;
            } else {
                $scope.largeDisplay = true;
                $scope.smallDisplay = false;
            }

            var isAgentDesktop = deviceDetector.isDesktop();

            if (isAgentDesktop) {
                $scope.opts.touchToDrag = false;
            } else {
                $scope.opts.touchToDrag = true;
            }
        };
        init();
        var w = angular.element($window);
        w.bind('resize', function () {
            $scope.$apply(function () {
                var newWidth = window.innerWidth;

                if (newWidth < 700) {
                    $scope.smallDisplay = true;
                } else if (newWidth < 1199) {
                    $scope.largeDisplay = false;
                } else {
                    $scope.largeDisplay = true;
                    $scope.smallDisplay = false;
                }
            });
        });

        // Close Snap Drawer On State Change
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            snapRemote.getSnapper().then(function (snapper) {
                snapper.close();
            });
        });
        // endregion

    }
]);